﻿using Microsoft.JSInterop;

namespace BlazorResources.Helpers
{
    public class FileHelpers
    {
        public async static Task SaveAs(IJSRuntime js, string filename, byte[] data)
        {
            await js.InvokeAsync<object>(
              "saveAsFile",
              filename,
              Convert.ToBase64String(data));
        }
    }
}
